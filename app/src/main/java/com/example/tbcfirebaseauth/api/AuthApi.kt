package com.example.tbcfirebaseauth.api

import com.example.tbcfirebaseauth.App
import com.example.tbcfirebaseauth.model.LogInResponse
import com.example.tbcfirebaseauth.model.SignUpResponse
import com.example.tbcfirebaseauth.model.User
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Query

interface AuthApi {

    @POST("/v1/accounts:signUp?")
    suspend fun signUp(
        @Body user: User,
        @Query("key")
        apiKey: String = App.apiKey
    ): Response<SignUpResponse>

    @POST("/v1/accounts:signInWithPassword?")
    suspend fun logIn(
        @Body user: User,
        @Query("key")
        apiKey: String = App.apiKey
    ):Response<LogInResponse>
}