package com.example.tbcfirebaseauth.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitService {
    const val BASE_URL = "https://identitytoolkit.googleapis.com"

    private val retrofit by lazy {
        Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create())
            .build()
    }
    val api: AuthApi by lazy {
        retrofit.create(AuthApi::class.java)
    }
}