package com.example.tbcfirebaseauth.fragment

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.tbcfirebaseauth.R
import com.example.tbcfirebaseauth.databinding.FragmentSignupBinding
import com.example.tbcfirebaseauth.model.User
import com.example.tbcfirebaseauth.vm.SignUpViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SignUpFragment : BaseFragment<FragmentSignupBinding, SignUpViewModel>(
    FragmentSignupBinding::inflate,
    SignUpViewModel::class.java
) {
    override fun start(layoutInflater: LayoutInflater, viewGroup: ViewGroup?) {
        init()
    }

    private fun init() {
        binding.btnSignUp.setOnClickListener {
            viewLifecycleOwner.lifecycleScope.launch {
                signUp()
            }
        }
    }

    private suspend fun signUp() {
        val email = binding.etEmail.text.toString()
        val pass = binding.etPassword.text.toString()
        if (email.isNotBlank() && pass.isNotBlank()) {
            val user =User(email, pass)
            viewModel.signUp(user)
            delay(1500)
            findNavController().navigate(R.id.action_signUpFragment_to_loginFragment)
            Toast.makeText(requireContext(), "successfully sign up", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(requireContext(), "please fill all field", Toast.LENGTH_SHORT).show()
        }
    }
}