package com.example.tbcfirebaseauth.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding

typealias Inflate<T> = (LayoutInflater, ViewGroup?, Boolean) -> T

abstract class BaseFragment<VB : ViewBinding, VM : ViewModel>(
    private val inflate: Inflate<VB>,
    private val vm: Class<VM>
) : Fragment() {
    private var _binding: VB? = null
    protected val binding get() = _binding!!

    protected val viewModel: VM by lazy { ViewModelProvider(this).get(vm) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (_binding == null) {
            _binding = inflate(layoutInflater, container, false)
            start(inflater, container)
        }
        return binding.root
    }

    abstract fun start(layoutInflater: LayoutInflater, viewGroup: ViewGroup?)

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}