package com.example.tbcfirebaseauth.fragment

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.tbcfirebaseauth.R
import com.example.tbcfirebaseauth.databinding.FragmentLoginBinding
import com.example.tbcfirebaseauth.model.User
import com.example.tbcfirebaseauth.vm.LoginViewModel

class LoginFragment : BaseFragment<FragmentLoginBinding, LoginViewModel>(
    FragmentLoginBinding::inflate,
    LoginViewModel::class.java
) {
    override fun start(layoutInflater: LayoutInflater, viewGroup: ViewGroup?) {
        init()
    }

    private fun init() {
        binding.btnLogIn.setOnClickListener {
            logIn()
        }
        binding.btnOpenSignUp.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_signUpFragment)
        }
    }

    private fun logIn() {
        val email = binding.etEmail.text.toString()
        val password = binding.etPassword.text.toString()
        if (email.isNotBlank() && password.isNotBlank()) {
            val user= User(email,password)
            viewModel.logIn(user)
            Toast.makeText(requireContext(), "successfully", Toast.LENGTH_SHORT).show()
        }else{
            Toast.makeText(requireContext(), "please fill all field", Toast.LENGTH_SHORT).show()
        }
    }
}