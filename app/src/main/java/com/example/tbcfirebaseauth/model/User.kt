package com.example.tbcfirebaseauth.model

data class User(
    val email:String?,
    val password:String?,
    val returnSecureToken: Boolean? = true
)
