package com.example.tbcfirebaseauth.vm

import android.util.Log.d
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.tbcfirebaseauth.api.RetrofitService
import com.example.tbcfirebaseauth.model.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SignUpViewModel : ViewModel() {

    fun signUp(user: User) = viewModelScope.launch(Dispatchers.IO) {
        val response = RetrofitService.api.signUp(user)
        d("response","${response.errorBody()}")
    }
}