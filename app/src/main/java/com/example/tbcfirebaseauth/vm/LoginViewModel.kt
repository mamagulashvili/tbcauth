package com.example.tbcfirebaseauth.vm

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.tbcfirebaseauth.api.RetrofitService
import com.example.tbcfirebaseauth.model.LogInResponse
import com.example.tbcfirebaseauth.model.SignUpResponse
import com.example.tbcfirebaseauth.model.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response

class LoginViewModel : ViewModel() {
    val logIn:MutableLiveData<Response<LogInResponse>> = MutableLiveData()

    fun logIn(user: User) = viewModelScope.launch(Dispatchers.IO) {
        val response = RetrofitService.api.logIn(user)
        logIn.value = response
        Log.d("response", "${response.body()}")
    }
}