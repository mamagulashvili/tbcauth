package com.example.tbcfirebaseauth

import android.app.Application

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        apiKey = getApiKey()
    }
    private external fun getApiKey(): String

    companion object {
        // Used to load the 'native-lib' library on application startup.
        init {
            System.loadLibrary("native-lib")
        }
        lateinit var apiKey:String
    }

}